from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from shishiproject.shishiapp import api

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^shishiapp/', include('shishiproject.shishiapp.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
]
