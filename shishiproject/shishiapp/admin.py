from adminsortable.admin import SortableAdmin, SortableTabularInline, NonSortableParentAdmin
from django import forms
from django.contrib import admin
from django.utils.safestring import mark_safe
from .models import Brand, Concept, ProductItem, TryOnItem, Category, Finish, TryOnGroup, TryOnGroupOfLook, Style, Look, \
    ShiShiUser, StyleAsset, Glitter, Lense, Shape, Account, VtoServer, TryOnGroupOfLookHasTryOnItems

from simple_history.admin import SimpleHistoryAdmin



class AccountAdminForm(forms.ModelForm):

    class Meta:
        model = Account
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self.fields['account_has_brands'].queryset = Brand.objects.all()
        self.fields['account_has_brands_readonly'].queryset = Brand.objects.all()


class VtoServerInline(SortableTabularInline):
    exclude = ['created', 'last_updated']
    fields = ['name', 'server_url', 'server_key', 'admin_password']
    model = VtoServer




class AccountAdmin(SortableAdmin):
    form = AccountAdminForm
    list_display = ['name', 'slug', 'created', 'brands', 'brands_readonly']
    readonly_fields = ['created']
    filter_horizontal = ('account_has_brands', 'account_has_styles', 'account_has_brands_readonly')

    inlines = [
        VtoServerInline,
    ]

    def brands(self, obj):
        brand_names = ''
        for brand in obj.account_has_brands.all():
            brand_names += brand.name + '\n'
        return brand_names

    def brands_readonly(self, obj):
        brand_names = ''
        for brand in obj.account_has_brands_readonly.all():
            brand_names += brand.name + '\n'
        return brand_names

admin.site.register(Account, AccountAdmin)






class ConceptInline(SortableTabularInline):
    exclude = ['created', 'last_updated']
    model = Concept
    readonly_fields = ['name', 'slug', 'tag_line','image', 'image_tag', 'description', 'ingredient_effect', 'howto', 'weight', 'concept_of_category', 'single_tryon', 'tags', 'tryon_image']
    show_change_link = True


class BrandAdminForm(forms.ModelForm):

    class Meta:
        model = Brand
        fields = '__all__'


class BrandAdmin(SortableAdmin):
    form = BrandAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'image', 'image_tag']
    readonly_fields = ['created', 'last_updated', 'image_tag']
    inlines = [
        ConceptInline,
    ]

admin.site.register(Brand, BrandAdmin)



class ConceptAdminForm(forms.ModelForm):

    class Meta:
        model = Concept
        fields = '__all__'


class ProductItemInline(SortableTabularInline):
    exclude = ['created', 'last_updated']
    readonly_fields = ['image_tag', 'tryon_image_tag']
    model = ProductItem
    show_change_link = True





class ConceptAdmin(SimpleHistoryAdmin, NonSortableParentAdmin):
    form = ConceptAdminForm
    list_display = ['name', 'slug', 'categories', 'tag_line', 'created', 'last_updated', 'single_tryon', 'image', 'tryon_image', 'description', 'ingredient_effect', 'howto', 'weight', 'url', 'image_tag', 'tryon_image_tag', 'created_by']
    readonly_fields = ['created', 'last_updated', 'image_tag']
    inlines = [
        ProductItemInline,
    ]

    def categories(self, obj):
        category_names = ''
        for category in obj.concept_of_category.all():
            category_names += category.name + '\n'
        return category_names


admin.site.register(Concept, ConceptAdmin)


class TryOnItemInline(SortableTabularInline):
    exclude = ['created', 'last_updated']
    fields = ['name', 'color_code', 'tryon_item_of_finish', 'parameter', 'color_name', 'finish_name', 'tryon_item_of_glitter', 'second_color_code', 'second_color_amount', 'tryon_item_of_lense', 'tryon_image', 'coverage']
    model = TryOnItem
    show_change_link = True

class ProductItemAdminForm(forms.ModelForm):

    class Meta:
        model = ProductItem
        fields = '__all__'


class TryOnGroupInline(SortableTabularInline):
    exclude = ['created', 'last_updated']
    readonly_fields = []
    model = TryOnGroup
    show_change_link = True


class ShapeAdminForm(forms.ModelForm):

    class Meta:
        model = Shape
        fields = '__all__'

class ShapeAdmin(admin.ModelAdmin):
    form = ShapeAdminForm

    list_display = ['name', 'slug', 'created', 'last_updated', 'curve_factor1', 'curve_factor2' ]
    readonly_fields = ['created', 'last_updated']

admin.site.register(Shape, ShapeAdmin)


class ProductItemAdmin(SimpleHistoryAdmin, NonSortableParentAdmin):
    form = ProductItemAdminForm

    list_display = ['name', 'created', 'last_updated', 'color', 'image', 'tryon_image', 'description', 'url',  'image_tag', 'tryon_image_tag', 'product_item_of_shape', 'weight', 'created_by', 'disable_auto_tryongroup']
    readonly_fields = ['created', 'last_updated', 'image_tag', 'tryon_image_tag']

    inlines = [
        TryOnItemInline,
        TryOnGroupInline,
    ]

admin.site.register(ProductItem, ProductItemAdmin)


class TryOnItemAdminForm(forms.ModelForm):

    class Meta:
        model = TryOnItem
        fields = '__all__'


class TryOnItemAdmin(SimpleHistoryAdmin):
    form = TryOnItemAdminForm
    list_display = ['name', 'created', 'last_updated', 'color_code', 'second_color_code', 'second_color_amount', 'color_name', 'finish_name', 'tryon_image', 'tryon_image_tag' ,'coverage', 'parameter', 'created_by']
    #readonly_fields = ['name', 'slug', 'created', 'last_updated', 'color_code', 'color_name', 'finish_name', 'coverage']

admin.site.register(TryOnItem, TryOnItemAdmin)


class CategoryAdminForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = '__all__'


class CategoryAdmin(SortableAdmin):
    form = CategoryAdminForm
    list_display = ['name', 'slug', 'super_category', 'created', 'last_updated']
    readonly_fields = ['created', 'last_updated']

admin.site.register(Category, CategoryAdmin)


class FinishAdminForm(forms.ModelForm):

    class Meta:
        model = Finish
        fields = '__all__'


class FinishAdmin(SimpleHistoryAdmin):
    form = FinishAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'glossy_level', 'gloss_spread', 'shimmer_level', 'glitter_level', 'glitter_amount', 'matte_level', 'coverage', 'lum_level', 'conceal_level', 'finish_of_brand', 'created_by']
    readonly_fields = ['created', 'last_updated']

admin.site.register(Finish, FinishAdmin)


class GlitterAdminForm(forms.ModelForm):

    class Meta:
        model = Glitter
        fields = '__all__'


class GlitterAdmin(admin.ModelAdmin):
    form = GlitterAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'glitter_level', 'glitter_amount', 'glitter_size', 'glitter_color_code']
    readonly_fields = ['created', 'last_updated']

admin.site.register(Glitter, GlitterAdmin)


class LenseAdminForm(forms.ModelForm):

    class Meta:
        model = Lense
        fields = '__all__'


class LenseAdmin(admin.ModelAdmin):
    form = LenseAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'opacity', 'specular_level', 'reflection_level', 'refraction_level', 'shininess', 'brightness']
    readonly_fields = ['created', 'last_updated']

admin.site.register(Lense, LenseAdmin)



class TryOnGroupAdminForm(forms.ModelForm):

    class Meta:
        model = TryOnGroup
        fields = '__all__'


class TryOnGroupTryOnItemInline(SortableTabularInline):
    exclude = ['created', 'last_updated']
    readonly_fields = []
    model = TryOnGroup.tryon_group_has_items.through
    show_change_link = True

    def item_name(self, obj):
        return obj.tryon_item.name

class TryOnGroupStyleInline(SortableTabularInline):
    exclude = ['created', 'last_updated']
    readonly_fields = []
    model = TryOnGroup.tryon_group_has_styles.through
    show_change_link = True

    def style_name(self, obj):
        return obj.style.name


class TryOnGroupAdmin(NonSortableParentAdmin):
    form = TryOnGroupAdminForm
    list_display = ['name', 'created', 'last_updated', 'items', 'created_by']
    readonly_fields = ['created', 'last_updated']
    inlines = [
        TryOnGroupStyleInline
    ]

admin.site.register(TryOnGroup, TryOnGroupAdmin)




class TryOnGroupOfLookTryOnItemInline(SortableTabularInline):
    exclude = ['created', 'last_updated']
    readonly_fields = []
    raw_id_fields = ('tryon_item',)
    model = TryOnGroupOfLookHasTryOnItems
    show_change_link = True

    def item_name(self, obj):
        return obj.tryon_item.name



class TryOnGroupOfLookAdminForm(forms.ModelForm):

    class Meta:
        model = TryOnGroupOfLook
        fields = '__all__'


class TryOnGroupOfLookAdmin(NonSortableParentAdmin):
    form = TryOnGroupOfLookAdminForm
    list_display = ['name', 'created', 'last_updated', 'items', 'styles', 'parameter', 'tryon_group_of_style']
    readonly_fields = ['created', 'last_updated']
    inlines = [
        TryOnGroupOfLookTryOnItemInline,
    ]

admin.site.register(TryOnGroupOfLook, TryOnGroupOfLookAdmin)








class StyleHasAssetInline(SortableTabularInline):
    exclude = ['created', 'last_updated']
    readonly_fields = ['style', 'asset_slug', 'asset_flag', 'asset_thickness', 'asset_image_tag']
    model = Style.style_has_assets.through
    show_change_link = True

    def asset_slug(self, obj):
        return obj.style_asset.slug

    def asset_image_tag(self, obj):
        return mark_safe(obj.style_asset.image_tag())

    def asset_flag(self, obj):
        return obj.style_asset.flag

    def asset_thickness(self, obj):
        return obj.style_asset.thickness


class StyleAdminForm(forms.ModelForm):

    class Meta:
        model = Style
        fields = '__all__'


class StyleAdmin(SortableAdmin):
    form = StyleAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'image', 'number_of_colors', 'asset_color_map', 'style_of_category', 'image_tag']
    readonly_fields = ['created', 'last_updated', 'image_tag']
    inlines = [
        StyleHasAssetInline,
    ]

admin.site.register(Style, StyleAdmin)



class StyleAssetAdminForm(forms.ModelForm):

    class Meta:
        model = StyleAsset
        fields = '__all__'


class StyleAssetAdmin(admin.ModelAdmin):
    form = StyleAssetAdminForm
    list_display = ['slug', 'created', 'last_updated', 'image', 'image_tag', 'alpha_factor']
    readonly_fields = ['created', 'last_updated', 'image_tag']

admin.site.register(StyleAsset, StyleAssetAdmin)



class LookAdminForm(forms.ModelForm):

    class Meta:
        model = Look
        fields = '__all__'


class TryOnGroupOfLookInline(SortableTabularInline):
    exclude = ['created', 'last_updated']
    readonly_fields = []
    model = TryOnGroupOfLook
    show_change_link = True

class LookAdmin(NonSortableParentAdmin):
    form = LookAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'detail', 'tag_line', 'intro', 'created_by', 'image', 'image_tag', 'quick_share', 'published', 'deleted', 'look_of_account']
    readonly_fields = ['created', 'last_updated', 'image_tag']
    inlines = [
        TryOnGroupOfLookInline,
    ]

admin.site.register(Look, LookAdmin)



class ShiShiUserAccountInline(SortableTabularInline):
    fields = ['account', 'role']
    readonly_fields = []
    model = ShiShiUser.user_of_account.through
    show_change_link = True



class ShiShiUserAdminForm(forms.ModelForm):

    class Meta:
        model = ShiShiUser
        fields = '__all__'


class ShiShiUserAdmin(NonSortableParentAdmin):
    form = ShiShiUserAdminForm
    list_display = ['user_name', 'display_name', 'user', 'account_list']
    inlines = [ShiShiUserAccountInline]

    def account_list(self, obj):
        account_names = ''
        for account in obj.user_of_account.all():
            account_names += account.name + '\n'
        return account_names


admin.site.register(ShiShiUser, ShiShiUserAdmin)

