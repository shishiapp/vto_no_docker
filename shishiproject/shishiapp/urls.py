from django.conf.urls import url
from . import api, admin_api


DEFAULT_VERSION = 'v(?P<version>[0-9]+)'

urlpatterns = ()

urlpatterns += (
    url(r'^admin/restore/$', admin_api.RestoreView.as_view()),
)


urlpatterns += (
    url(r'^api/' + DEFAULT_VERSION + '/db_timestamp/$', api.DBTimeStampView.as_view()),
)

urlpatterns += (
    url(r'^api/' + DEFAULT_VERSION + '/brands/$', api.BrandList.as_view(), name='api_brand_list'),
)

urlpatterns += (
    url(r'^api/' + DEFAULT_VERSION + '/concept_info/$', api.ConceptInfoList.as_view(), name='api_concept_info_list'),
    url(r'^api/' + DEFAULT_VERSION + '/look_info/$', api.LookInfoList.as_view(), name='api_look_info_list'),
)

urlpatterns += (
    url(r'^api/' + DEFAULT_VERSION + '/categories/$', api.CategoryList.as_view(), name='api_category_list'),
)

urlpatterns += (
    url(r'^api/' + DEFAULT_VERSION + '/brand/(?P<brand_id>\d+)/category/(?P<category_id>\d+)/concepts/$', api.ConceptList.as_view(), name='api_concept_list'),
    url(r'^api/' + DEFAULT_VERSION + '/concepts/$', api.ConceptRetrieve.as_view()),
    url(r'^api/' + DEFAULT_VERSION + '/concept/$', api.ConceptView.as_view()),
    url(r'^api/' + DEFAULT_VERSION + '/concept/(?P<pk>\d+)/$', api.ConceptView.as_view())
)



urlpatterns += (
    url(r'^api/' + DEFAULT_VERSION + '/concept/(?P<pk>\d+)/product_items/$', api.ProductItemList.as_view(), name='api_product_item_list'),
    url(r'^api/' + DEFAULT_VERSION + '/product_items/$', api.ProductItemRetrieve.as_view()),
    url(r'^api/' + DEFAULT_VERSION + '/tryon_items/$', api.TryOnItemRetrieve.as_view()),
    url(r'^api/' + DEFAULT_VERSION + '/styles/$', api.StyleRetrieve.as_view())
)

urlpatterns += (
    url(r'^api/' + DEFAULT_VERSION + '/product_item/(?P<pk>\d+)/$', api.ProductItemView.as_view()),
    url(r'^api/' + DEFAULT_VERSION + '/product_item/(?P<product_item_id>\d+)/tryon_items/$', api.TryOnItemList.as_view(), name='api_tryon_item_list'),
)

urlpatterns += (
    url(r'^api/' + DEFAULT_VERSION + '/look/(?P<look_id>\d+)/tryon_groups/$', api.TryOnGroupsOfLookList.as_view(), name='api_tryon_groups_of_look_list'),
    url(r'^api/' + DEFAULT_VERSION + '/tryon_groups_of_look/$', api.TryOnGroupsOfLookFetch.as_view(), name='api_tryon_groups_of_look_fetch'),

)

urlpatterns += (
    url(r'^api/' + DEFAULT_VERSION + '/product_item/(?P<product_item_id>\d+)/tryon_groups/$', api.TryOnGroupsOfProductItemList.as_view(), name='api_tryon_groups_of_product_item_list'),
)

urlpatterns += (
    url(r'^api/' + DEFAULT_VERSION + '/look/(?P<pk>[0-9]+)/$', api.LookDetail.as_view()),
)

urlpatterns += (
    url(r'^api/' + DEFAULT_VERSION + '/styles/$', api.StyleList.as_view()),
    url(r'^api/' + DEFAULT_VERSION + '/style/(?P<pk>[0-9]+)/$', api.StyleDetail.as_view()),
)

urlpatterns += (
    url(r'^api/' + DEFAULT_VERSION + '/product_mapping/$', api.ProductMapping.as_view()),
)