from . import models
from .versioning import *
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from django.db.models import Prefetch
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.models import AnonymousUser


def getLooks(pk=None, type=None, request=None):

    ids = request.GET.get('ids')

    if ids is not None:
        ids = ids.split(',')
        looks = models.Look.objects.filter(pk__in=ids)

        serializerClass = getLookSerializerClass(request)
        serializer = serializerClass(looks, many=True, context={'request': request})

        return serializer


    # get all looks

    if (settings.SHISHI_ACCOUNT_ID == 2):
        # public VTO server

        looks = models.Look.objects.none()

        if not isinstance(request.user, AnonymousUser):
            for account in request.user.shishiuser.user_of_account.all():
                looks = looks | models.Look.objects.filter(look_of_account=account)

        looks = looks | models.Look.objects.filter(deleted=False, published=True, look_of_account=2)

    else:
        # private VTO server
        looks = models.Look.objects.filter(deleted=False, published=True, look_of_account=settings.SHISHI_ACCOUNT_ID)



    limit = request.GET.get('limit')
    page = request.GET.get('page')
    if limit is not None:
        paginator = Paginator(looks, limit)
        page = 1
    else:
        paginator = Paginator(looks, 7)

    try:
        looks = paginator.page(page)
    except PageNotAnInteger:
        looks = paginator.page(1)
    except EmptyPage:
        looks = []

    serializerClass = getLookSerializerClass(request)
    serializer = serializerClass(looks, many=True, context={'request': request})

    return serializer

class ProductMapping(APIView):
    def get(self, request, pk=None, type=None, version=None):
        serializer = serializers.ProductMappingSerializer(models.ProductItem.objects.all(), many=True)
        return Response(serializer.data)

class LookList(APIView):
    def get(self, request, pk=None, type=None, version=None):
        serializer = getLooks(pk, type, request)

        return Response(serializer.data)


class BrandList(APIView):

    def get(self, request, version=None):
        brands = models.Brand.objects.prefetch_related('brand_of_account').all()

        serializerClass = getBrandSerializerClass(request)

        serializer = serializerClass(brands, many=True, context={'request':request})
        return Response(serializer.data)


class LookDetail(APIView):
    def get(self, request, pk=None, version=None):

        look = models.Look.objects.prefetch_related('tryon_groups', 'tryon_groups__tryon_group_of_style', 'tryon_groups__tryon_group_of_look_has_items').prefetch_related(Prefetch(
            'tryon_groups__tryon_group_of_style__style_has_assets',
            queryset=models.StyleAsset.objects.filter(min_version__lte=int(version), max_version__gte=int(version)).order_by('asset_of_style__rank'))).get(pk=pk)

        serializerClass = getLookSerializerClass(request)
        serializer = serializerClass(look, context={'request':request})
        return Response(serializer.data)


class StyleDetail(APIView):
    def get(self, request, pk=None, version=None):

        style = models.Style.objects.get(pk=pk)

        serializer = serializers.StyleSerializer(style, context={'request':request})

        return Response(serializer.data)


class ConceptInfoList(APIView):

    def get(self, request, version=None):
        concepts = models.Concept.objects.filter(hidden=False).prefetch_related('concept_of_category')
        serializer = serializers.ConceptInfoSerializer(concepts, many=True, context={'request': request})
        return Response(serializer.data)


class LookInfoList(APIView):

    def get(self, request, version=None):
        looks = models.Look.objects.filter(published=True, deleted=False)

        serializer = serializers.LookInfoSerializer(looks, many=True, context={'request':request})
        return Response(serializer.data)


class ConceptRetrieve(APIView):

    def get(self, request, version=None):
        ids = request.GET.get('ids')
        ids = ids.split(',')
        concepts = models.Concept.objects.filter(pk__in=ids)\
            .prefetch_related('concept_of_category', 'product_items')
        serializerClass = getConceptSerializerClass(request)
        serializer = serializerClass(concepts, many=True, context={'request':request})
        return Response(serializer.data)


class ConceptView(APIView):
    def get(self, request, pk=None, version=None):
        if pk is not None:
            concept = models.Concept.objects.get(pk=pk)
        else:
            product_item_id = request.GET.get('product_item')
            concept = models.ProductItem.objects.get(pk=product_item_id).product_item_of_concept

        serializerClass = getConceptSerializerClass(request)
        serializer = serializerClass(concept, context={'request':request})
        return Response(serializer.data)



class ProductItemView(APIView):
    def get(self, request, pk=None, version=None):

        productItem = models.ProductItem.objects \
            .prefetch_related(Prefetch('product_item_of_concept__concept_of_category', to_attr='categories'))\
            .prefetch_related('tryon_items', 'tryon_items__tryon_item_of_finish') \
            .prefetch_related('tryon_groups', 'tryon_groups__tryongrouphasstyles_set',
                              'tryon_groups__tryongrouphasstyles_set__style') \
            .prefetch_related(Prefetch('tryon_groups__tryongrouphasstyles_set__style__style_has_assets',
                                       queryset=models.StyleAsset.objects.filter(min_version__lte=int(version),
                                                                                 max_version__gte=int(version)).order_by('asset_of_style__rank')))\
            .get(pk=pk)

        serializerClass = getProductItemSerializerClass(request)
        serializer = serializerClass(productItem, context={'request':request})
        return Response(serializer.data)


class ProductItemRetrieve(APIView):

    def get(self, request, version=None):
        ids = request.GET.get('ids')
        ids = ids.split(',')
        productItems = models.ProductItem.objects.filter(pk__in=ids)\
            .prefetch_related(Prefetch('product_item_of_concept__concept_of_category', to_attr='categories'))\
            .prefetch_related('tryon_items', 'tryon_items__tryon_item_of_finish')\
            .prefetch_related('tryon_groups', 'tryon_groups__tryongrouphasstyles_set', 'tryon_groups__tryongrouphasstyles_set__style') \
            .prefetch_related(Prefetch('tryon_groups__tryongrouphasstyles_set__style__style_has_assets',
            queryset=models.StyleAsset.objects.filter(min_version__lte=int(version), max_version__gte=int(version)).order_by('asset_of_style__rank')))


        serializerClass = getProductItemSerializerClass(request)
        serializer = serializerClass(productItems, many=True, context={'request':request})
        return Response(serializer.data)



class TryOnItemRetrieve(APIView):

    def get(self, request, version=None):
        ids = request.GET.get('ids')
        ids = ids.split(',')
        tryOnItems = models.TryOnItem.objects.filter(pk__in=ids)

        serializerClass = getTryOnItemSerializerClass(request)
        serializer = serializerClass(tryOnItems, many=True, context={'request':request})
        return Response(serializer.data)


class StyleRetrieve(APIView):

    def get(self, request, version=None):
        ids = request.GET.get('ids')
        ids = ids.split(',')
        styles = models.TryOnItem.objects.filter(pk__in=ids)
        serializer = serializers.StyleSerializer(styles, many=True)
        return Response(serializer.data)


class ConceptList(APIView):

    def get(self, request, brand_id, category_id, version=None):
        concepts = models.Concept.objects.filter(concept_of_brand = brand_id, concept_of_category = category_id, hidden=False).prefetch_related('concept_of_category', 'product_items')

        serializerClass = getConceptSerializerClass(request)
        serializer = serializerClass(concepts, many=True, context={'request':request})
        return Response(serializer.data)


class ProductItemList(APIView):
    def get(self, request, pk, version=None):
        product_items = models.ProductItem.objects.filter(product_item_of_concept = pk, hidden=False)\
            .prefetch_related(Prefetch('product_item_of_concept__concept_of_category', to_attr='categories'))\
            .prefetch_related('tryon_items', 'tryon_items__tryon_item_of_finish')\
            .prefetch_related('tryon_groups', 'tryon_groups__tryongrouphasstyles_set', 'tryon_groups__tryongrouphasstyles_set__style') \
            .prefetch_related(Prefetch('tryon_groups__tryongrouphasstyles_set__style__style_has_assets',
            queryset=models.StyleAsset.objects.filter(min_version__lte=int(version), max_version__gte=int(version)).order_by('asset_of_style__rank')))

        serializerClass = getProductItemSerializerClass(request)
        serializer = serializerClass(product_items, many=True, context={'request':request})
        return Response(serializer.data)



class TryOnItemList(APIView):
    def get(self, request, product_item_id, version=None):
        tryon_items = models.TryOnItem.objects.filter(tryon_item_of_product_item = product_item_id)

        serializerClass = getTryOnItemSerializerClass(request)
        serializer = serializerClass(tryon_items, many=True, context={'request':request})
        return Response(serializer.data)



class CategoryList(APIView):

    def get(self, request, version=None):
        categories = models.Category.objects.all().prefetch_related('styles').prefetch_related(Prefetch(
            'styles__style_has_assets',
            queryset=models.StyleAsset.objects.filter(min_version__lte=int(version), max_version__gte=int(version)).order_by('asset_of_style__rank')))

        serializerClass = getCategorySerializerClass(request)
        serializer = serializerClass(categories, many=True, context={'request':request})
        return Response(serializer.data)


class StyleList(APIView):
    def get(self, request, version=None):
        styles = models.Style.objects.filter()
        serializer = serializers.StyleSerializer(styles, many=True, context={'request':request})
        return Response(serializer.data)

class TryOnGroupsOfProductItemList(APIView):
    def get(self, request, product_item_id, version=None):
        tryon_groups = models.TryOnGroup.objects.filter(tryon_group_of_product_item = product_item_id)
        serializer = serializers.TryOnGroupSerializer(tryon_groups, many=True, context={'request':request})
        return Response(serializer.data)


class TryOnGroupsOfLookList(APIView):
    def get(self, request, look_id, version=None):
        tryon_groups = models.TryOnGroupOfLook.objects.filter(tryon_group_of_look = look_id)
        serializer = serializers.TryOnGroupOfLookSerializer(tryon_groups, many=True, context={'request':request})
        return Response(serializer.data)



class TryOnGroupsOfLookFetch(APIView):
    permission_classes = (permissions.AllowAny,)
    def post(self, request, version=None):
        groups = request.data["tryon_groups"]

        tryon_groups = []

        for group in groups:
            tryon_group = models.TryOnGroupOfLook()
            tryon_group.pk = 0
            tryon_group.category_id = group['category']
            tryon_group.tryon_group_of_style_id = group['style']
            tryon_group.parameter = group['parameter']

            tryon_group = serializers.TryOnGroupOfLookSerializer(tryon_group, context={'request': request}).data

            for item in group['items']:
                tryon_item = models.TryOnItem.objects.filter(pk=item)
                if tryon_item.exists():
                    serializerClass = getTryOnItemSerializerClass(request)
                    tryon_item = serializerClass(tryon_item.first(), context={'request': request}).data
                    tryon_group['tryon_item_objects'].append(tryon_item)

            tryon_groups.append(tryon_group)

        return Response(tryon_groups)


class DBTimeStampView(APIView):

    def get(self, request, version=None):
        db_timestamp = 0
        migration = models.ShiShiMigration.objects.all().first()

        if migration is not None:
            db_timestamp = int(migration.timestamp.timestamp())

        return Response({'timestamp': db_timestamp}, status=status.HTTP_200_OK)



