from adminsortable.fields import SortableForeignKey
from adminsortable.models import SortableMixin
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from django.db import models as models
from django_extensions.db import fields as extension_fields
from taggit.managers import TaggableManager
from versatileimagefield.fields import VersatileImageField
from simple_history.models import HistoricalRecords
from django.utils.safestring import mark_safe


class VtoServer(SortableMixin):
    name = models.CharField(max_length=255, blank=True)
    server_url = models.URLField(blank=True)
    server_key = models.CharField(max_length=255, blank=True)
    admin_password = models.CharField(max_length=100, default='', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    server_of_account =  models.ForeignKey('Account', related_name='servers', on_delete=models.CASCADE)

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)

    class Meta:
        ordering = ('rank',)

    def __unicode__(self):
        return u'%s' % self.name

    def __str__(self):
        return u'%s' % self.name



class Account(models.Model):
    name = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=100, unique=True)

    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    account_has_brands = models.ManyToManyField('shishiapp.Brand', related_name='brand_of_account', blank=True)
    account_has_brands_readonly = models.ManyToManyField('shishiapp.Brand', related_name='brand_visible_for_account', blank=True)
    account_has_styles = models.ManyToManyField('shishiapp.Style')
    tags = TaggableManager(blank=True)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def __str__(self):
        return u'%s' % self.name


class Brand(SortableMixin):

    # Fields
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=100, unique=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    image = VersatileImageField(upload_to="images/brand/")
    tags = TaggableManager(blank=True)
    hidden = models.BooleanField(default=False)

    owned_by = models.ForeignKey('shishiapp.Account', null=True, blank=True, on_delete=models.SET_NULL)


    class Meta:
        ordering = ('rank',)

    def __str__(self):
        return u'%s' % self.name

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_brand_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('shishiapp_brand_update', args=(self.slug,))

    def image_tag(self):
        return mark_safe('<img width="150" height="150" src="%s" />' % self.image.url)

    image_tag.short_description = 'Image'

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)


class Concept(SortableMixin):

    # Fields
    name = models.CharField(max_length=255)
    original_name = models.CharField(max_length=255, blank=True)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    tag_line = models.CharField(max_length=200, blank=True)
    single_tryon = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    description = models.TextField(max_length=1000, blank=True)
    ingredient_effect = models.TextField(max_length=1000, blank=True)
    howto = models.TextField(max_length=1000, blank=True)
    weight = models.CharField(max_length=100, blank=True)
    image = VersatileImageField(upload_to="images/concept/")
    tryon_image = VersatileImageField(upload_to="images/tryon_image/", blank=True)
    url = models.URLField(blank=True)
    history = HistoricalRecords()

    created_by = models.IntegerField(blank=True, null=True)

    tags = TaggableManager(blank=True)
    hidden = models.BooleanField(default=False)

    # Relationship Fields
    concept_of_brand = SortableForeignKey('shishiapp.Brand', on_delete=models.CASCADE)
    concept_of_category = models.ManyToManyField('shishiapp.Category', )

    class Meta:
        ordering = ('rank',)

    def __str__(self):
        return u'%s' % self.name

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_concept_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('shishiapp_concept_update', args=(self.slug,))

    def image_tag(self):
        return mark_safe('<img width="150" height="150" src="%s" />' % self.image.url)

    def tryon_image_tag(self):
        if (self.tryon_image):
            return mark_safe('<img width="150" height="150" src="%s" />' % self.tryon_image.url)
        else:
            return u''

    tryon_image_tag.short_description = 'TryOnImage'

    image_tag.short_description = 'Image'

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)



#tryongroup of productitem
class TryOnGroup(SortableMixin):

    # Fields
    name = models.CharField(max_length=255, blank=True)
    slug = extension_fields.AutoSlugField(populate_from='tryon_group_of_product_item', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    items = models.TextField(max_length=500, blank=True)
    category = models.ForeignKey('shishiapp.Category', on_delete=models.CASCADE)

    created_by = models.IntegerField(blank=True, null=True)

    tryon_group_of_product_item = models.ForeignKey('shishiapp.ProductItem', related_name='tryon_groups', on_delete=models.CASCADE)

    # Relationship Fields

    tryon_group_has_items = models.ManyToManyField('shishiapp.TryOnItem', through='TryOnGroupHasTryOnItems')
    tryon_group_has_styles = models.ManyToManyField('shishiapp.Style', through='TryOnGroupHasStyles')


    class Meta:
        ordering = ('rank',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_tryongroup_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('shishiapp_tryongroup_update', args=(self.slug,))

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)


#tryongroup of look
class TryOnGroupOfLook(SortableMixin):

    # Fields
    name = models.CharField(max_length=255, blank=True)
    slug = extension_fields.AutoSlugField(populate_from='tryon_group_of_look', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    items = models.TextField(max_length=500, blank=True)
    styles = models.TextField(max_length=500, blank=True)
    parameter = models.IntegerField(blank=True, null=True)

    # should remove blank = True, null = True
    tryon_group_of_style = models.ForeignKey('shishiapp.Style', related_name='tryon_groups', blank=True, null=True, on_delete=models.SET_NULL)

    category = models.ForeignKey('shishiapp.Category', on_delete=models.CASCADE)
    tryon_group_of_look = models.ForeignKey('shishiapp.Look', related_name='tryon_groups', on_delete=models.CASCADE)

    # Relationship Fields

    tryon_group_of_look_has_items = models.ManyToManyField('shishiapp.TryOnItem', through='TryOnGroupOfLookHasTryOnItems')

    class Meta:
        ordering = ('rank',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_tryongroupoflook_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('shishiapp_tryongroupoflook_update', args=(self.slug,))

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)

    def get_tryon_items(self):
        return self.tryon_group_of_look_has_items.order_by('tryon_group').select_related('tryon_item_of_finish')



class ProductItem(SortableMixin):

    # Fields
    name = models.CharField(max_length=255, blank=True)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    color = models.CharField(max_length=100, blank=True)
    image = VersatileImageField(upload_to="images/product_item/", blank=True)
    tryon_image = VersatileImageField(upload_to="images/tryon_image/", blank=True)
    product_id = models.CharField(max_length=100, blank=True)
    description = models.TextField(max_length=2000, blank=True)
    url = models.URLField(blank=True)
    weight = models.CharField(max_length=100, blank=True)
    tags = TaggableManager(blank=True)
    hidden = models.BooleanField(default=False)
    history = HistoricalRecords()
    disable_auto_tryongroup = models.BooleanField(default=False)

    created_by = models.IntegerField(blank=True, null=True)

    product_item_of_shape = models.ForeignKey('shishiapp.Shape', blank=True, null=True, on_delete=models.SET_NULL)

    # Relationship Fields
    product_item_of_concept = SortableForeignKey('shishiapp.Concept', related_name='product_items', on_delete=models.CASCADE)


    class Meta:
        ordering = ('rank',)

    def __str__(self):
        return u'%s' % self.name

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_productitem_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('shishiapp_productitem_update', args=(self.pk,))

    def tryon_image_tag(self):
        if (self.tryon_image):
            return mark_safe('<img width="150" height="150" src="%s" />' % self.tryon_image.url)
        else:
            return u''

    tryon_image_tag.short_description = 'TryOnImage'

    def image_tag(self):
        if (self.image):
            return mark_safe('<img width="150" height="150" src="%s" />' % self.image.url)
        else:
            return u''

    image_tag.short_description = 'Image'

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)

    def get_is_hidden(self):
        return (self.hidden or self.product_item_of_concept.hidden)


class Shape(models.Model):
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    curve_factor1 = models.FloatField(default=0.0)
    curve_factor2 = models.FloatField(default=0.0)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return u'%s (%s)' % (self.name, self.slug)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_frame_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('shishiapp_frame_update', args=(self.slug,))


class Lense(models.Model):
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    specular_level = models.FloatField(default=0.0)
    reflection_level = models.FloatField(default=0.0)
    refraction_level = models.FloatField(default=0.0)
    opacity = models.FloatField(default=0.0)
    shininess = models.FloatField(default=0.0)
    brightness = models.FloatField(default=1.0)

    lense_of_asset = models.ForeignKey('shishiapp.StyleAsset', null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return u'%s (%s)' % (self.name, self.slug)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_lense_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('shishiapp_lense_update', args=(self.slug,))


class TryOnItem(SortableMixin):

    # Fields
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    color_code = models.CharField(max_length=100)
    second_color_code = models.CharField(max_length=100, blank=True)
    second_color_amount = models.FloatField(default=0.0)
    color_name = models.CharField(max_length=100, blank=True)
    finish_name = models.CharField(max_length=100, blank=True)
    parameter = models.IntegerField(default=0)
    coverage = models.FloatField(default=0.0)
    tryon_image = VersatileImageField(upload_to="images/tryon_image/", blank=True)
    history = HistoricalRecords()

    created_by = models.IntegerField(blank=True, null=True)

    # Relationship Fields
    tryon_item_of_product_item = SortableForeignKey('shishiapp.ProductItem', related_name='tryon_items', on_delete=models.CASCADE)
    tryon_item_of_finish = models.ForeignKey('shishiapp.Finish', blank=True, null=True, on_delete=models.SET_NULL)
    tryon_item_of_glitter = models.ForeignKey('shishiapp.Glitter', blank=True, null=True, on_delete=models.SET_NULL)

    tryon_item_of_lense = models.ForeignKey('shishiapp.Lense', null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ('rank',)

    def __str__(self):
        return u'%s' % self.pk + '  (' + self.tryon_item_of_product_item.name + ' - ' + self.name + ')'

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_tryonitem_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('shishiapp_tryonitem_update', args=(self.slug,))

    def tryon_image_tag(self):
        if (self.tryon_image):
            return mark_safe('<img width="150" height="150" src="%s" />' % self.tryon_image.url)
        else:
            return u''

    tryon_image_tag.short_description = 'TryOnImage'

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)


SUPER_CATEGORY_CHOICES = (
    ('face', 'Face'),
    ('eye', 'Eye'),
    ('lip', 'Lip'),
    ('eyebrow', 'Eyebrow'),
    ('glasses', 'Glasses'),
    ('mask', 'Mask'),
    ('jewelry', 'Jewelry'),
)


class Category(SortableMixin):

    # Fields
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=100, unique=True)
    super_category = models.CharField(max_length=100, choices=SUPER_CATEGORY_CHOICES)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)


    class Meta:
        ordering = ('rank',)

    def __str__(self):
        return u'%s' % self.name

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_category_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('shishiapp_category_update', args=(self.slug,))

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)


class Glitter(models.Model):
    # Fields
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    glitter_level = models.FloatField(default=0.0)
    glitter_amount = models.FloatField(default=0.0)
    glitter_size = models.FloatField(default=1.0)
    glitter_color_code = models.CharField(max_length=100, blank=True)


    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return u'%s (%s)' % (self.name, self.slug)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_glitter_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('shishiapp_glitter_update', args=(self.slug,))




class Finish(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    display_name = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    glossy_level = models.FloatField(default=0.0, blank=True, null=True)
    gloss_spread = models.FloatField(default=0.0, blank=True, null=True)
    shimmer_level = models.FloatField(default=0.0, blank=True, null=True)
    glitter_level = models.FloatField(default=0.0, blank=True, null=True)
    matte_level = models.FloatField(default=0.0, blank=True, null=True)
    glitter_amount = models.FloatField(default=0.0, blank=True, null=True)
    coverage = models.FloatField(default=0.0, blank=True, null=True)
    lum_level = models.FloatField(default=0.0, blank=True, null=True)
    conceal_level = models.FloatField(default=0.0, blank=True, null=True)
    history = HistoricalRecords()

    created_by = models.IntegerField(default=0, blank=True, null=True)

    # Relationship Fields
    finish_of_category = models.ManyToManyField('shishiapp.Category')
    finish_of_brand = models.ForeignKey('shishiapp.Brand', blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return u'%s (%s)' % (self.name, self.slug)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_finish_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('shishiapp_finish_update', args=(self.slug,))







class Style(SortableMixin):

    # Fields
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=100, unique=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    image = VersatileImageField(upload_to="images/style/")
    enabled = models.BooleanField(default=False)
    number_of_colors = models.IntegerField(default=0)
    asset_color_map = models.CharField(max_length=30, blank=True)
    parameters = models.CharField(max_length=100, blank=True)

    style_has_assets = models.ManyToManyField('shishiapp.StyleAsset', through='StyleHasAssets')

    # Relationship Fields
    style_of_category = models.ForeignKey('shishiapp.Category', related_name='styles', on_delete=models.CASCADE)

    class Meta:
        ordering = ('rank',)

    def __str__(self):
        return u'%s (%s)' % (self.name, self.slug)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_style_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('shishiapp_style_update', args=(self.slug,))


    def image_tag(self):
        return mark_safe('<img width="170" height="120" src="%s" />' % self.image.url)

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)

    image_tag.short_description = 'Image'

    def get_assets(self):
        return self.style_has_assets.order_by('asset_of_style')





class StyleAsset(SortableMixin):

    slug = models.CharField(max_length=100, unique=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    image = VersatileImageField(upload_to="images/style_asset/")
    flag = models.IntegerField()
    thickness = models.IntegerField(default=0)
    alpha_factor = models.FloatField(default=1.0)
    max_version = models.IntegerField(default=1000000)
    min_version = models.IntegerField(default=1)

    class Meta:
        ordering = ('rank',)

    def __str__(self):
        return u'%s' % (self.slug)


    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_style_asset_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('shishiapp_style_asset_update', args=(self.slug,))

    def image_tag(self):
        return mark_safe('<img width="170" height="120" src="%s"/>' % self.image.url)

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)

    image_tag.short_description = 'Image'



class StyleHasAssets(SortableMixin):
    style = models.ForeignKey(Style, on_delete=models.CASCADE)
    style_asset = models.ForeignKey(StyleAsset, related_name='asset_of_style', on_delete=models.CASCADE)

    class Meta:
        ordering = ('rank',)

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)


class TryOnGroupOfLookHasTryOnItems(SortableMixin):
    tryon_group = models.ForeignKey(TryOnGroupOfLook, on_delete=models.CASCADE)
    tryon_item = models.ForeignKey(TryOnItem, on_delete=models.CASCADE, related_name='tryon_group')

    class Meta:
        ordering = ('rank',)

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)


class TryOnGroupOfLookHasStyles(SortableMixin):
    tryon_group = models.ForeignKey(TryOnGroupOfLook, on_delete=models.CASCADE)
    style = models.ForeignKey(Style, on_delete=models.CASCADE)

    class Meta:
        ordering = ('rank',)

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)



class TryOnGroupHasTryOnItems(SortableMixin):
    tryon_group = models.ForeignKey(TryOnGroup, on_delete=models.CASCADE)
    tryon_item = models.ForeignKey(TryOnItem, on_delete=models.CASCADE)

    class Meta:
        ordering = ('rank',)

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)


class TryOnGroupHasStyles(SortableMixin):
    tryon_group = models.ForeignKey(TryOnGroup, on_delete=models.CASCADE)
    style = models.ForeignKey(Style, on_delete=models.CASCADE, related_name='tryon_group')
    parameter = models.IntegerField(blank=True, null=True)

    class Meta:
        ordering = ('rank',)

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)


class Look(SortableMixin):

    # Fields
    name = models.CharField(max_length=255)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    tag_line = models.CharField(max_length=200, blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    detail = models.TextField(max_length=500, blank=True)
    intro = models.TextField(max_length=2000, blank=True)
    image = VersatileImageField(upload_to="images/look/", blank=True)
    published = models.BooleanField(default=False)
    quick_share = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)
    tags = TaggableManager(blank=True)
    history = HistoricalRecords()


    # Relationship Fields
    created_by = models.IntegerField(blank=True, null=True)

    look_of_account =  models.ForeignKey('Account', related_name='looks', blank=True, null=True, on_delete=models.SET_NULL)

    def image_tag(self):
        return mark_safe('<img width="150" height="150" src="%s" />' % (self.image.url if self.image else ''))

    class Meta:
        ordering = ('rank',)

    def __str__(self):
        return u'%s' % self.name

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('shishiapp_look_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('shishiapp_look_update', args=(self.pk,))


    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)

    image_tag.short_description = 'Image'

    def get_is_hidden(self):
        return (not self.published or self.deleted)





class UserOfAccount(SortableMixin):
    PERMISSIONS = (
        ('C', 'Contributor'),
        ('S', 'Supervisor'),
        ('A', 'Admin'),
        ('O', 'Observer'),
    )
    role = models.CharField(max_length=1, choices=PERMISSIONS)
    user = models.ForeignKey('shishiapp.ShiShiUser', on_delete=models.CASCADE)
    account = models.ForeignKey('shishiapp.Account', on_delete=models.CASCADE)

    class Meta:
        ordering = ('rank',)

    rank = models.PositiveIntegerField(default=0, editable=False, db_index=True)


class ShiShiUser(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='shishiuser')

    display_name = models.TextField(max_length=100)

    intro = models.TextField(max_length=100, blank=True)
    birthdate = models.DateTimeField(editable=True, blank=True, null=True)
    state = models.TextField(max_length=50, blank=True)
    city = models.TextField(max_length=50, blank=True)
    area = models.TextField(max_length=50, blank=True)
    hidden = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    image = VersatileImageField(upload_to="images/user/", blank=True, null=True)
    image_large = VersatileImageField(upload_to="images/user/", blank=True, null=True)


    user_of_account = models.ManyToManyField('shishiapp.Account', through='shishiapp.UserOfAccount', related_name='users')


    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return u'%s' % self.user.username

    def __unicode__(self):
        return u'%s' % self.user.username

    def user_name(self):
        return self.user.username

    def user_id(self):
        return self.user.id

    def get_absolute_url(self):
        return reverse('shishiapp_shishiuser_detail', args=(self.user.username,))


    def get_update_url(self):
        return reverse('shishiapp_shishiuser_update', args=(self.user.username,))

class ShiShiMigration(models.Model):
    name = models.CharField(max_length=255)
    timestamp = models.DateTimeField(auto_now=True)
