import json

from django.conf import settings
from rest_framework import serializers

from shishiproject.shishiapp import apps

from . import models

class ShapeSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Shape
        fields = (
            'pk',
            'slug',
            'created',
            'last_updated',
            'curve_factor1',
            'curve_factor2',
        )

class StyleAssetSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.StyleAsset
        fields = (
            'pk',
            'slug',
            'image',
            'created',
            'flag',
            'thickness',
            'alpha_factor',
            'min_version',
            'max_version',
        )

class BrandSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Brand
        fields = (
            'pk',
            'slug', 
            'name', 
            'created', 
            'last_updated',
            'brand_of_account',
            'image',
            'rank',
        )


class TimedSerializer(serializers.ModelSerializer):
    db_timestamp = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super(TimedSerializer, self).__init__(*args, **kwargs)
        if apps.db_timestamp == 0:

            migration = models.ShiShiMigration.objects.all().first()
            if migration is not None:
                apps.db_timestamp = int(migration.timestamp.timestamp())

    def get_db_timestamp(self, obj):
        return apps.db_timestamp


class LastUpdatedTimedSerializer(serializers.ModelSerializer):
    obj_timestamp = serializers.SerializerMethodField()

    def get_obj_timestamp(self, obj):
        obj_timestamp = int(obj.last_updated.timestamp())
        return obj_timestamp





class ConceptSerializer(TimedSerializer, LastUpdatedTimedSerializer):

    product_item_count = serializers.IntegerField(source='product_items.count', read_only=True)

    class Meta:
        model = models.Concept
        fields = (
            'pk',
            'slug', 
            'name',
            'tag_line',
            'created', 
            'last_updated', 
            'image',
            'single_tryon',
            'description',
            'ingredient_effect',
            'howto',
            'url',
            'weight',
            'concept_of_brand',
            'concept_of_category',
            'product_item_count',
            'rank',
            'hidden',
            'tryon_image',
            'created_by',
            'db_timestamp',
            'obj_timestamp',
        )

    def get_tags(self, look):
        tags = sorted(look.tags.names())
        return tags



class LookInfoSerializer(LastUpdatedTimedSerializer):

    class Meta:
        model = models.Concept
        fields = (
            'pk',
            'name',
            'obj_timestamp'
        )



class ConceptInfoSerializer(LastUpdatedTimedSerializer):

    class Meta:
        model = models.Concept
        fields = (
            'pk',
            'concept_of_brand',
            'concept_of_category',
            'hidden'
        )

class GlitterSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Glitter
        fields = (
            'pk',
            'slug',
            'name',
            'created',
            'last_updated',
            'glitter_size',
            'glitter_color_code',
            'glitter_level',
            'glitter_amount',
        )

class LenseSerializer(serializers.ModelSerializer):

    asset = StyleAssetSerializer(source='lense_of_asset', many=False, read_only=True)

    class Meta:
        model = models.Lense
        fields = (
            'pk',
            'slug',
            'name',
            'created',
            'last_updated',
            'specular_level',
            'reflection_level',
            'refraction_level',
            'opacity',
            'asset',
            'shininess',
            'brightness',
        )


class FinishSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Finish
        fields = (
            'pk',
            'slug',
            'name',
            'display_name',
            'created',
            'last_updated',
            'glossy_level',
            'gloss_spread',
            'shimmer_level',
            'glitter_level',
            'matte_level',
            'glitter_amount',
            'coverage',
            'lum_level',
            'conceal_level',
            'created_by'
        )

class ProductMappingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProductItem
        fields = (
            'product_id',
            'pk'
        )


class TryOnItemSerializer(serializers.ModelSerializer):
    finish = FinishSerializer(source='tryon_item_of_finish')
    glitter = GlitterSerializer(source='tryon_item_of_glitter')
    lense = LenseSerializer(source="tryon_item_of_lense")


    class Meta:
        model = models.TryOnItem
        fields = (
            'pk',
            'name', 
            'created', 
            'last_updated', 
            'color_code',
            'second_color_code',
            'second_color_amount',
            'color_name', 
            'finish_name', 
            'coverage',
            'tryon_item_of_product_item',
            'finish',
            'glitter',
            'parameter',
            'rank',
            'lense',
            'tryon_image',
            'tryon_item_of_finish',
            'tryon_item_of_glitter',
            'tryon_item_of_lense',
            'created_by'
        )


class StyleSerializer(serializers.ModelSerializer):

    style_assets = StyleAssetSerializer(source='style_has_assets', many=True)

    class Meta:
        model = models.Style
        fields = (
            'pk',
            'slug',
            'name',
            'created',
            'last_updated',
            'image',
            'rank',
            'style_assets',
            'number_of_colors',
            'asset_color_map',
            'parameters'
        )


class TryOnGroupHasStyleSerializer(serializers.ModelSerializer):
    style_object = StyleSerializer(source='style')

    class Meta:
        model = models.TryOnGroupHasStyles
        fields = (
            'style',
            'style_object',
            'parameter',
        )

class TryOnGroupSerializer(serializers.ModelSerializer):
    style_parameters = TryOnGroupHasStyleSerializer(source='tryongrouphasstyles_set', many=True)

    class Meta:
        model = models.TryOnGroup
        fields = (
            'pk',
            'name',
            'created',
            'last_updated',
            'items',
            'category',
            'tryon_group_of_product_item',
            'rank',
            'style_parameters',
        )



class ProductItemSerializer(TimedSerializer, LastUpdatedTimedSerializer):
    tryon_items = TryOnItemSerializer(many=True)
    tryon_groups = TryOnGroupSerializer(many=True)
    shape = ShapeSerializer(source='product_item_of_shape')

    image = serializers.SerializerMethodField()
    categories = serializers.SerializerMethodField()

    class Meta:
        model = models.ProductItem
        fields = (
            'pk',
            'slug',
            'name',
            'created',
            'last_updated',
            'image',
            'tryon_image',
            'color',
            'description',
            'categories',
            'url',
            'product_id',
            'product_item_of_concept',
            'product_item_of_shape',
            'tryon_items',
            'tryon_groups',
            'rank',
            'shape',
            'created_by',
            'db_timestamp',
            'obj_timestamp',
            'hidden',
            'disable_auto_tryongroup',
        )

    def get_image(self, product_item):
        if product_item.image:
            return product_item.image.url
        else:
            return product_item.product_item_of_concept.image.url


    def get_categories(self, productItem):
        return [category.id for category in productItem.product_item_of_concept.categories]


class CategorySerializer(TimedSerializer, LastUpdatedTimedSerializer):

    styles = StyleSerializer(many=True, read_only=True)

    class Meta:
        model = models.Category
        fields = (
            'pk',
            'slug', 
            'name',
            'super_category',
            'created', 
            'last_updated',
            'rank',
            'styles',
        )


class SimpleUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ShiShiUser
        fields = (
            'pk',
            'user_id',
            'user_name',
            'display_name',
            'image',
        )


class SimpleLookSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Look
        fields = (
            'pk',
            'slug',
            'name',
            'image',
            'rank',
            'intro',
            'created_by',
            'published',
        )


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ShiShiUser
        fields = (
            'pk',
            'user_id',
            'user_name',
            'display_name',
            'image',
            'image_large',
            'intro',
            'birthdate',
            'state',
            'city',
            'area',
        )

class TryOnGroupOfLookSerializer(serializers.ModelSerializer):

    tryon_item_objects = TryOnItemSerializer(source='get_tryon_items', many=True)
    style = StyleSerializer(source='tryon_group_of_style')

    class Meta:
        model = models.TryOnGroupOfLook
        fields = (
            'pk',
            'name',
            'created',
            'last_updated',
            'items',
            'styles',
            'parameter',
            'category',
            'tryon_group_of_look',
            'tryon_item_objects',
            'rank',
            'tryon_group_of_style',
            'style',
        )



class LookSerializer(TimedSerializer, LastUpdatedTimedSerializer):

    tryon_groups = TryOnGroupOfLookSerializer(many=True)

    class Meta:
        model = models.Look
        fields = (
            'pk',
            'slug',
            'name',
            'tag_line',
            'created',
            'last_updated',
            'detail',
            'intro',
            'image',
            'rank',
            'created_by',
            'tryon_groups',
            'published',
            'deleted',
            'quick_share',
            'db_timestamp',
            'obj_timestamp'
        )

    def create(self, validated_data):
        tryon_groups = validated_data.pop('tryon_groups')
        look = models.Look.objects.create(**validated_data)

        for tryon_group in tryon_groups:
            items = json.loads(tryon_group['items'])

            group = models.TryOnGroupOfLook.objects.create(tryon_group_of_look=look, **tryon_group)

            for pk in items:
                tryon_item = models.TryOnItem.objects.get(pk=pk)
                models.TryOnGroupOfLookHasTryOnItems.objects.create(tryon_item=tryon_item, tryon_group=group)


        return look

    def update(self, look, validated_data):

        tryon_groups = validated_data.get('tryon_groups', None)

        if tryon_groups is None or len(tryon_groups) == 0:
            look.name = validated_data.get('name', look.name)
            look.tag_line = validated_data.get('tag_line', look.tag_line)
            look.detail = validated_data.get('detail', look.detail)
            look.intro = validated_data.get('intro', look.intro)
            look.published = validated_data.get('published', look.published)
            look.image = validated_data.get('image', look.image)
            look.deleted = validated_data.get('deleted', look.deleted)
            look.quick_share = validated_data.get('quick_share', look.quick_share)
            look.save()

        else:
            models.TryOnGroupOfLook.objects.filter(tryon_group_of_look=look).delete()

            for tryon_group in tryon_groups:
                items = json.loads(tryon_group['items'])

                group = models.TryOnGroupOfLook.objects.create(tryon_group_of_look=look, **tryon_group)

                for pk in items:
                    tryon_item = models.TryOnItem.objects.get(pk=pk)
                    models.TryOnGroupOfLookHasTryOnItems.objects.create(tryon_item=tryon_item, tryon_group=group)

            look.save()

        return look