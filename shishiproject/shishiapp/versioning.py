from . import serializers
from django.conf import settings


def getUserSerializerClass(request):
    serializerClass = serializers.UserSerializer
    return serializerClass


def getActionSerializerClass(request):
    serializerClass = serializers.ActionSerializer
    return serializerClass



def getLookSerializerClass(request):

    serializerClass = serializers.LookSerializer

    return serializerClass





def getConceptSerializerClass(request):

    serializerClass = serializers.ConceptSerializer

    return serializerClass


def getProductItemSerializerClass(request):

    serializerClass = serializers.ProductItemSerializer

    return serializerClass


def getCategorySerializerClass(request):

    serializerClass = serializers.CategorySerializer

    return serializerClass



def getTryOnItemSerializerClass(request):
    serializerClass = serializers.TryOnItemSerializer

    return serializerClass



def getBrandSerializerClass(request):
    serializerClass = serializers.BrandSerializer

    return serializerClass