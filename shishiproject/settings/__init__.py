import os
import cbs
from django.utils.translation import gettext_lazy as _

try:
    from shishiproject.settings.local_settings import *
except ImportError:
    pass


class BaseSettings():
    PROJECT_NAME = 'shishiproject'

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    DATA_UPLOAD_MAX_NUMBER_FIELDS = 1500

    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.sites',
        'django.contrib.sitemaps',
        'django.contrib.postgres',
        'autoslug',
        'corsheaders',
        'django_extensions',
        'crispy_forms',
        'rest_framework',
        'shishiproject.shishiapp.apps.ShiShiAppConfig',
        'adminsortable',
        'taggit',
        'simple_history',
        'storages',
    ]

    EXTENSIONS_MAX_UNIQUE_QUERY_ATTEMPTS = 1000

    SITE_ID = 1

    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    )

    MIDDLEWARE = [
        'corsheaders.middleware.CorsMiddleware',
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.locale.LocaleMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
        'simple_history.middleware.HistoryRequestMiddleware',
    ]

    INTERNAL_IPS = (
        '127.0.0.1'
    )

    CORS_URLS_REGEX = r'^.*$'

    CORS_ORIGIN_REGEX_WHITELIST = (
        r'^(https?://)?(\w+\.)?google\.com$'
        r'^(https?://)?(\w+\.)?shishiapp\.com$'
        r'^(https?://)?(\w+\.)?localhost$'
    )

    ROOT_URLCONF = 'shishiproject.urls'

    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                    'django.template.context_processors.static',
                    'django.template.context_processors.i18n',
                ],
            },
        },
    ]

    WSGI_APPLICATION = 'shishiproject.wsgi.application'

    AUTH_PASSWORD_VALIDATORS = [
        {
            'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
        },
    ]

    AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend',
    )

    AUTH_USER_MODEL = 'auth.User'


    REST_FRAMEWORK = {
        'DEFAULT_PERMISSION_CLASSES': (
            'rest_framework.permissions.IsAuthenticatedOrReadOnly',
        ),

        'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.URLPathVersioning'
    }


    TIME_ZONE = 'UTC'

    USE_I18N = True

    USE_L10N = True

    USE_TZ = True

    LANGUAGES = (
        ('en', _('English')),
    )

    LOCALE_PATHS = (
        os.path.join(os.path.dirname(os.path.dirname(__file__)), "shishiapp/locale"),
    )


    STATIC_ROOT = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static")
    STATIC_URL = '/static/'


    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': {
            'file': {
                'level': 'WARNING',
                'class': 'logging.FileHandler',
                'filename': 'warning.log',
            },
        },
        'loggers': {
            'django': {
                'handlers': ['file'],
                'level': 'WARNING',
                'propagate': True,
            },
        },
    }


class LocalSettings(BaseSettings):
    pass


MODE = os.environ.get('DJANGO_MODE', 'Local').title()
cbs.apply('{}Settings'.format(MODE), globals())